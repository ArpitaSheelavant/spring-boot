package com.practice;

public class FamilyDetails {
	
	private String type;
	private String familyMember;
	private int diagnosticAge;
	private String condition;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFamilyMember() {
		return familyMember;
	}
	public void setFamilyMember(String familyMember) {
		this.familyMember = familyMember;
	}
	public int getDiagnosticAge() {
		return diagnosticAge;
	}
	public void setDiagnosticAge(int diagnosticAge) {
		this.diagnosticAge = diagnosticAge;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	
	

}
