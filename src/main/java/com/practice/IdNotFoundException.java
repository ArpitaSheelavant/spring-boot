package com.practice;

import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IdNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3892106125299402496L;

	public IdNotFoundException(String e ){
		super(e);
		System.out.println("--here2--");
	}

}
