package com.practice;

import java.util.List;

public class MedicalInfo {
	
	private int height;
	private List<FamilyDetails> familyDetails;
	private List<QuestionSet> questionSet;
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public List<FamilyDetails> getFamilyDetails() {
		return familyDetails;
	}
	public void setFamilyDetails(List<FamilyDetails> familyDetails) {
		this.familyDetails = familyDetails;
	}
	public List<QuestionSet> getQuestionSet() {
		return questionSet;
	}
	public void setQuestionSet(List<QuestionSet> questionSet) {
		this.questionSet = questionSet;
	}
	
	

}
