package com.practice;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler  {
	
	@ExceptionHandler(IdNotFoundException.class)
	 public ResponseEntity<ErrorDetails> handleIdNotFoundException(IdNotFoundException e,HttpServletRequest req){
		
		
		ErrorDetails ED=new ErrorDetails();
		
		ED.setTimestamp(new Date());
		ED.setMessage(e.getMessage());
		ED.setErrorCode(HttpStatus.NOT_FOUND.value());
		ED.setPath(req.getRequestURI());
		System.out.println("--here3--");
		
		return new ResponseEntity<>(ED,HttpStatus.NOT_FOUND);
		
	}

} 
