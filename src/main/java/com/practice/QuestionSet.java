package com.practice;

public class QuestionSet {
	
	private String question;
	private String userSelectedValue;
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getUserSelectedValue() {
		return userSelectedValue;
	}
	public void setUserSelectedValue(String userSelectedValue) {
		this.userSelectedValue = userSelectedValue;
	}
	
	

}
