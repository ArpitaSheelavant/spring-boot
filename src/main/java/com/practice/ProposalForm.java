package com.practice;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Proposal")
public class ProposalForm {

	@Id
	private String proposalNumber;
	private String version;
	private ApplicationDetails applicationDetails;
	private SourcingDetails sourcingDetails;
	private List<PartyDetails> partyDetails;
	private ProductInfo productInfo;
	private MedicalInfo medicalInfo;
	private PaymentDetails paymentDetails;
	private Bankdetails bankDetails;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ApplicationDetails getApplicationDetails() {
		return applicationDetails;
	}

	public String getProposalNumber() {
		return proposalNumber;
	}

	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}

	public void setApplicationDetails(ApplicationDetails applicationDetails) {
		this.applicationDetails = applicationDetails;
	}

	public SourcingDetails getSourcingDetails() {
		return sourcingDetails;
	}

	public void setSourcingDetails(SourcingDetails sourcingDetails) {
		this.sourcingDetails = sourcingDetails;
	}

	public List<PartyDetails> getPartyDetails() {
		return partyDetails;
	}

	public void setPartyDetails(List<PartyDetails> partyDetails) {
		this.partyDetails = partyDetails;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public MedicalInfo getMedicalInfo() {
		return medicalInfo;
	}

	public void setMedicalInfo(MedicalInfo medicalInfo) {
		this.medicalInfo = medicalInfo;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public Bankdetails getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(Bankdetails bankDetails) {
		this.bankDetails = bankDetails;
	}

}
