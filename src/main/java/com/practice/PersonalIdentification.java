package com.practice;

import java.util.List;

public class PersonalIdentification {
	
	private List<Phone> phone;
	private Aadhar aadhar;
	private EnrollmentNo enrollment;
	public List<Phone> getPhone() {
		return phone;
	}
	public void setPhone(List<Phone> phone) {
		this.phone = phone;
	}
	public Aadhar getAadhar() {
		return aadhar;
	}
	public void setAadhar(Aadhar aadhar) {
		this.aadhar = aadhar;
	}
	public EnrollmentNo getEnrollment() {
		return enrollment;
	}
	public void setEnrollment(EnrollmentNo enrollment) {
		this.enrollment = enrollment;
	}
	
	

}
