package com.practice;

public class PartyDetails {
	
	private String type;
	private BasicDetails basicDetails;
	private PersonalIdentification personalIdentification;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BasicDetails getBasicDetails() {
		return basicDetails;
	}
	public void setBasicDetails(BasicDetails basicDetails) {
		this.basicDetails = basicDetails;
	}
	public PersonalIdentification getPersonalIdentification() {
		return personalIdentification;
	}
	public void setPersonalIdentification(PersonalIdentification personalIdentification) {
		this.personalIdentification = personalIdentification;
	}
	
}
