package com.practice;

public class Bankdetails {
	
	private String mICR;
	private String iFSC;
	private String accountNumber;
	private String bankName;
	
	
	public String getmICR() {
		return mICR;
	}
	public void setmICR(String mICR) {
		this.mICR = mICR;
	}
	public String getiFSC() {
		return iFSC;
	}
	public void setiFSC(String iFSC) {
		this.iFSC = iFSC;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	
	

}
