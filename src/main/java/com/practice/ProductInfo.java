package com.practice;

public class ProductInfo {
	
	private String productId;
	private int termofProduct;
	private double sumAssured;
	private double modalPremium;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public int getTermofProduct() {
		return termofProduct;
	}
	public void setTermofProduct(int termofProduct) {
		this.termofProduct = termofProduct;
	}
	public double getSumAssured() {
		return sumAssured;
	}
	public void setSumAssured(double sumAssured) {
		this.sumAssured = sumAssured;
	}
	public double getModalPremium() {
		return modalPremium;
	}
	public void setModalPremium(double modalPremium) {
		this.modalPremium = modalPremium;
	}
	
	
}
