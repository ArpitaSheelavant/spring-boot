package com.practice;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/store")
public class StoreController {
	
	@Autowired
	private TestRepository testRepository;
	
	@Autowired
	private MongoTemplate mongo;
	
	@PostMapping
	public String store(@RequestBody ProposalForm proposalForm){
		
		ProposalForm pf=testRepository.save(proposalForm);
		return "successfull";
	}
	
	@GetMapping(value="/{id}",produces ="application/json")
	public ProposalForm getData(@PathVariable String id) throws IdNotFoundException{
		
		ProposalForm PF=testRepository.findById(id).orElseThrow(() -> 
		new IdNotFoundException("The requested Id " +id+" Document is not Present"));
		return PF;
	}
	
	@GetMapping(value="/number={number}")
	public List<ProposalForm> getByNumber(@PathVariable String number){
		
		List<ProposalForm> PF=testRepository.findByPhone(number);
		return PF;
	}
	
	@GetMapping(value="/aadharNumber={aadharNumber}")
	public ProposalForm getByAadhar(@PathVariable String aadharNumber){
		
		ProposalForm PF=testRepository.findByAadhar(aadharNumber);
		return PF;
	}
	
	@GetMapping(value="/{number}/{aadharNumber}")
	public List<ProposalForm> getByParam(@PathVariable  String number,
			@PathVariable String aadharNumber ){
		
		Query dq=new Query();
		if(number!=null){
			Criteria c1=Criteria.where("partyDetails.personalIdentification.phone.number").is(number);
			dq.addCriteria(c1);
		}
		if(aadharNumber!=null){
			Criteria c2=Criteria.where("partyDetails.personalIdentification.aadhar.aadharNumber").is(aadharNumber);
			dq.addCriteria(c2);
		}
		List<ProposalForm> PF=mongo.find(dq,ProposalForm.class);
	
		return PF;
	}
}
