package com.practice;

public class SourcingDetails {
	
	private String agentId;
	private String agentName;
	private String agentLoc;
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentLoc() {
		return agentLoc;
	}
	public void setAgentLoc(String agentLoc) {
		this.agentLoc = agentLoc;
	}
	
	

}
