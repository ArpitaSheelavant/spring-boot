package com.practice;

public class Phone {
	private String number;
	private String type;
	private String sTDCODE;
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getsTDCODE() {
		return sTDCODE;
	}
	public void setsTDCODE(String sTDCODE) {
		this.sTDCODE = sTDCODE;
	}

}
