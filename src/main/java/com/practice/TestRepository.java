package com.practice;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
//import org.springframework.data.querydsl.QuerydslPredicateExecutor;


public interface TestRepository extends MongoRepository<ProposalForm,String> {
	
	 @Query("{'partyDetails.personalIdentification.phone.number': ?0}")
	 List<ProposalForm> findByPhone(String number);
	 
	 @Query("{'partyDetails.personalidentification.aadhar.aadharNumber': ?0}")
	 ProposalForm findByAadhar(String aadhar);

}
